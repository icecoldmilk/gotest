package main

import (
	// "gotest/mathhelper"
	// "gotest/stringhelper"
	"fmt"
	"runtime"
)

func main() {
	// mathhelper.Sqrt(3)
	// stringhelper.Myprint()
	// stringhelper.Hehe()
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.", os)
	}
	defer fmt.Println("world")

	fmt.Println("hello")
}
